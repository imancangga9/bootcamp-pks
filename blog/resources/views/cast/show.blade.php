@extends('layout.master')

@section('judul')
Detail Cast {{$cast->nama}}
@endsection

@section('content')
    <h2>{{$cast->nama}}</h2>
    <h2>{{$cast->umur}} tahun</h2>
    <p>{{$cast->bio}}</p>
@endsection