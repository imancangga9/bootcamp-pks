@extends('layout.master')

@section('judul')
Buat Account Baru  
@endsection

@section('content')
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf

        <label>First name:</label><br>
        <input type="text" id="fname" name="fname"><br><br>
        <label>Last Name</label><br>
        <input type="text" id="lname" name="lname"><br><br>
        <label>Gender :</label><br>
        <input type="radio" name="gender" value="gmale"> Male <br>
        <input type="radio" name="gender" value="gfemale"> Female <br>
        <input type="radio" name="gender" value="gother"> Other <br><br>
        <label>Nationality:</label><br><br>
        <select>
            <option value="nindonesia">Indonesa</option>
            <option value="namerika">Amerika</option>
            <option value="ninggris">Inggris</option>
        </select><br><br>
        <label>Langage Spoken:</label><br><br>
        <input type="checkbox" value="lsindonesa">Bahasa Indonesa<br>
        <input type="checkbox" value="lsenglish">English<br>
        <input type="checkbox" value="lsother">Other <br><br>
        <label>Bio</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        
        <input type="submit" value="Signup">
    </form>
@endsection