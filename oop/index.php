
<?php
    require_once('Animal.php');
    require_once('ape.php');
    require_once('frog.php');

    
    $sheep = new Animal("shaun");

    echo "Name : ". $sheep->name . "<br>"; // "shaun"
    echo "Legs : " . $sheep->legs . "<br>"; // 4
    echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

    $kodok = new Frog("buduk");
    echo "Name : ". $kodok->name . "<br>";
    echo "Legs : ". $kodok->legs . "<br>";
    echo "Cold Blooded : " . $sheep->cold_blooded . "<br>";
    echo $kodok->jump() . "<br><br>"; // "hop hop"

    $ape = new Ape("Ape");
    echo "Name : ". $ape->name . "<br>"; // "shaun"
    echo "Legs : " . $ape->legs . "<br>"; // 4
    echo "Cold Blooded : " . $ape->cold_blooded . "<br>"; // "no"
    echo $ape->yell() . "<br><br>";
?>